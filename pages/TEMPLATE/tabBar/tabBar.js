import { getTabBar } from '../../../services/API.js'

//初始化数据
function tabbarinit() {
  return [
    {
      "current": 0,
      "pagePath": "/pages/HOME/home/home",
      "iconPath": "/images/shouye-1.png",
      "selectedIconPath": "/images/shouye.png",
      "text": "首页"
    },
    {
      "current": 0,
      "pagePath": "/pages/TYPE/type/type",
      "iconPath": "/images/fenlei-1.png",
      "selectedIconPath": "/images/fenlei.png",
      "text": "分类"
    },
    // {
    //   "current": 0,
    //   "pagePath": "/pages/HOME/fenxiao/fenxiao",
    //   "iconPath": "/images/icon_fx_f.png",
    //   "selectedIconPath": "/images/icon_fx_t.png",
    //   "text": "分销"
    // },
    {
      "current": 0,
      "pagePath": "/pages/CART/cart/cart",
      "iconPath": "/images/gouwuche.png",
      "selectedIconPath": "/images/gouwuche-1.png",
      "text": "购物车"
    },
    {
      "current": 0,
      "pagePath": "/pages/USER/user/user",
      "iconPath": "/images/wode-1.png",
      "selectedIconPath": "/images/wode.png",
      "text": "我的"
    }
  ]
}

//tabbar 主入口
function tabbarmain(bindName = "tabdata", id, target) {
  var that = target;
  var bindData = {};
  var otabbar = tabbarinit();
  otabbar[id]['iconPath'] = otabbar[id]['selectedIconPath']//换当前的icon
  otabbar[id]['current'] = 1;
  bindData[bindName] = otabbar;
  that.setData({ bindData });
}

module.exports = {
  tabbar: tabbarmain
}
import { getCategory, getGoodsList } from '../../../services/API';
import * as wxAPI from '../../../services/wxAPI';
const app = getApp();
const template = require('../../TEMPLATE/tabBar/tabBar.js');
Page({
  data: {
    http: app.http,
    host: app.host,
    category: '',
    items: [],
    page: 0,
    isAgain: true,
    isNomore: false,
    active: 0,
    special: {
      showSpecial: false,
      list: ''
    },
    activeCategoryId: '',
    categoryIdx: 0,
    childIdx: 0,
    num: 2,
    nums: 2
  },
  onLoad(option) {
    template.tabbar("tabBar", 1, this);
    let special = this.data.special;
    special.list = app.specialList;
    this.setData({ special });

    getCategory().then(({ status, result, msg }) => {
      if (status == 1) {
        const items = this.data.items;
        this.setData({
          category: result.category,
          items: items.concat(result.list),
          page: ++this.data.page,
          isAgain: true,
        })
        this.finish(result.list);
        // console.log(JSON.stringify(result))
      }
    })

  },
  changeOil: function (e) {
    // console.log(e);
    this.setData({
      num: e.target.dataset.num,
      nums: 2
    })
  },
  changeOill: function (e) {
    // console.log(e);
    this.setData({
      nums: e.target.dataset.nums,
      num: 2
    })
  },
  //公用加载内容
  // getGoodsList(params) {
  //   getGoodsList(params).then(({status, result, msg}) => {
  //     if(status == 1) {
  //       const items = this.data.items;
  //       this.setData({
  //         items: items.concat(result),
  //         page: ++this.data.page,
  //         isAgain: true
  //       })
  //       this.finish(result);
  //     }
  //   })
  // }
  getGoodsList() {
    let that = this
    let dt = that.data
    // 加载一级内容
    console.log(dt)
    wxAPI.showLoading()
    let params = {
      page: dt.page,
      category_id: dt.category[dt.categoryIdx].child[dt.childIdx].id || dt.category[dt.categoryIdx].id
    }
    // console.log(params)
    // console.log(dt)
    // console.log(getGoodsList(params))
    getGoodsList(params).then(({
      status,
      result,
      msg
    }) => {
      if (status == 1) {
        const items = dt.items;
        if (dt.page > 0) {
          that.setData({
            items: items.concat(result),
            page: ++dt.page,
            isAgain: true
          })
        } else {
          that.setData({
            items: result,
            page: 1,
            isAgain: true
          })
        }
        that.finish(result)
        wxAPI.hideLoading()
      }
    })
  },
  //tab切换
  // switchTab(e) {
  //   const index = e.currentTarget.dataset.index;
  //   const id = e.currentTarget.dataset.id;
  //   this.reset();
  //   this.setData({ active: index, activeCategoryId: id });
  //   this.getGoodsList({ page: this.data.page, category_id: id })
  // },
  //二级tab
  tapCategory(e) {
    // console.log(e)
    let that = this
    let dt = that.data
    let idx = e.currentTarget.dataset.idx;
    // console.log(idx)
    // console.log(this.data.categoryIdx)
    if (this.data.categoryIdx != idx) {
      let childIdx = dt.category[idx].child.length >= 0 ? 0 : null;
      this.setData({
        categoryIdx: idx,
        childIdx: childIdx,
        page: 0
      })
      this.getGoodsList()
    }
  },
  tapList(e) {
    let that = this
    let dt = that.data
    let idx1 = e.currentTarget.dataset.idx1
    // console.log(idx1)
    this.setData({
      childIdx: idx1,
      page: 0
    })
    this.getGoodsList()
  },
  //滚动到底部
  onReachBottom() {
    if (!this.data.isAgain) return;
    this.setData({ isAgain: false });
    const categoryId = this.data.activeCategoryId;
    this.getGoodsList({ page: this.data.page, category_id: categoryId });
  },
  //重置函数
  reset() {
    this.setData({
      items: [],
      page: 0,
      isAgain: true,
      isNomore: false,
    })
  },
  //判断是否结束
  finish(arr) {
    if (arr.length < 10) {
      this.setData({
        isAgain: false,
        isNomore: true
      })
    }
  },
  onblur(e) {
    this.setData({
      keyword: e.detail.value
    })
  },
  oninput(e) {
    let keyword = e.detail.value
    if (keyword != '') {
      this.setData({
        isEmpt: true,
        keyword
      })
    } else {
      this.setData({
        isEmpt: false
      })
    }
    return keyword.trim()
  },
  empt() {
    setTimeout(() => {
      this.setData({
        keyword: '',
        isEmpt: false
      })
    }, 50)
  },
  search() {
    const keyword = this.data.keyword;
    if (keyword) {
      wx.navigateTo({
        url: `/pages/HOME/searchResult/searchResult?keyword=${keyword}`
      })
    }
  },
  //专题点击
  clickSpecial() {
    template.tabbar("tabBar", 2, this);
    let special = this.data.special;
    if (special.list.length == 0) {
      app.wxAPI.alert('专题暂未开启！')
        .then(() => {
          template.tabbar("tabBar", 1, this);
        })
      return;
    }
    special.showSpecial = true;
    this.setData({ special });
  },
  navigatorTo(e) {
    let url = e.currentTarget.dataset.url;
    const id = e.currentTarget.dataset.id;
    if (!url) {
      url = `/pages/SPECIAL/special/special?id=${id}`
    }
    wx.navigateTo({
      url: url
    })
    this.closePopup();
    template.tabbar("tabBar", 1, this);
  },
  closePopup() {
    let special = this.data.special;
    special.showSpecial = false;
    this.setData({ special });
    template.tabbar("tabBar", 1, this);
  }
})
// pages/HOME/tixian/tixian.js
import { accountindex } from '../../../services/API'
import { dalay } from '../../../utils/utils'

const app = getApp()
Page({
  /**
   * 页面的初始数据
   */
  data: {
    http: app.http,
    host: app.host,
    datatx: [],
  },
  // 提现明细
  accountindex(params) {
    //var that = this
    accountindex(params).then(({ status, result, message }) => {
      if (status == 0) {
        if (result =="") {
          app.wxAPI.alert('暂无提现订单')
        }
        let bankno = [];
        // result.data.map(function (e) {
        //   var nodata = e.bank_card.substr(e.bank_card.length - 4);
        //   e.nofour = nodata
        //   // bankno.push(nodata);
        // })
        //app.wxAPI.alert(message);
        console.log(result)
        this.setData({
          datatx:result
        })
      } else {
        app.wxAPI.alert(message);
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.accountindex();
    // var nums ="62170014399954430";
    // var nummiss = nums.substr(0, 4) + "********" + nums.substr(-4);
    // console.log(nummiss);

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
// pages/HOME/tixian/tixian.js
import { udistributapply, distributprepare } from '../../../services/API'
import { dalay } from '../../../utils/utils'

const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    http: app.http,
    host: app.host,
    datatx: [],
    deposit: ["一级分销商"],
    depositmun: [],
    dep: 0,
    type_index: 0,

  },

  bindsetChange: function (e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      type_index: e.detail.value,
      dep: this.data.depositmun[e.detail.value]
    })

  },

  //   准备
  distributprepare(params) {
    distributprepare(params).then(({ status, result, msg }) => {

      if (status == 1) {
        const depositmun = this.data.depositmun;
        depositmun.push(result.first_deposit);
        depositmun.push(result.second_deposit);
        depositmun.push(result.third_deposit);

        this.setData({
          depositmun: depositmun
        })
      } else {
        app.wxAPI.alert(msg);
      }
    })
  },

  //   申请
  udistributapply(params) {
    udistributapply(params).then(({ status, result, msg }) => {

      if (status == 1) {

        this.setData({

        })
      } else {
        app.wxAPI.alert(msg);
      }
    })
  },

  // 分销商申请信息查询
  udistributapplyInfo(params) {
    udistributapplyInfo(params).then(({ status, result, msg }) => {

      if (status == 1) {

        this.setData({

        })
      } else {
        app.wxAPI.alert(msg);
      }
    })
  },


  // 提交
  formSubmit(e) {
    if (e.detail.value.username == "") {
      app.wxAPI.alert('名字不能为空！');
    } else if (this.data.mobile == false) {
      app.wxAPI.alert('请确认协议！');

    } else if (this.data.certificate == "") {
      app.wxAPI.alert('资料不能为空！');

    } else {
      this.udistributapply({ username: e.detail.value.surname, mobile: e.detail.value.mobile, level: e.detail.value.mobile, deposit: this.data.dep });
    }



  },




  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.distributprepare();

    // this.udistributapply();
    // var nums ="62170014399954430";
    // var nummiss = nums.substr(0, 4) + "********" + nums.substr(-4);
    // console.log(nummiss);

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
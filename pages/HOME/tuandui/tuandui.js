import {
  distributsuperior
} from '../../../services/API'
import {
  dalay
} from '../../../utils/utils'

const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    http: app.http,
    host: app.host,
    distribut: {},
    orders: {},
    activeType: 0,
    teamnum: 0,
    teamList: [],
    Team: [],
    txalert: false,
    navs: [{
      title: '一级（0）人',
      type: '0',
    },
    // {
    //   title: '二级（0）人',
    //   type: '1',
    // },
    // {
    //   title: '三级（0）人',
    //   type: '2',
    // },

    ],
  },

  //团队
  indexTeam(params) {
    distributsuperior(params).then(({
      status,
      result,
      msg
    }) => {
      if (status == 1) {
        console.log(result)
        var navs = this.data.navs;
        navs[0].title = '一级(' + result.length + ')人';
        // navs[1].title = '二级(' + result.second.length + ')人';
        // navs[2].title = '三级(' + result.third.length + ')人';
        this.setData({
          teamList: result,
          //Team: result,
          navs: navs
        });
      } else {
        app.wxAPI.alert(msg)
      }
    })
  },
  tabList(e) {
    let token = app.token;
    if (!token) {
      app.wxAPI.alert('未登录!')
      return
    }
    const index = e.currentTarget.dataset.index;
    const items = this.data.items;
    wx.navigateTo({
      url: items[index].url
    })
  },
  tabNavBar(e) {
    let type = ''
    type = e.currentTarget.dataset.type;
    if (type == 0) {
      this.setData({
        activeType: type,
        teamList: this.data.Team.first
      });
    } else if (type == 1) {
      this.setData({
        activeType: type,
        teamList: this.data.Team.second
      });
    } else {
      this.setData({
        activeType: type,
        teamList: this.data.Team.third
      });
    }



  },
  // 分销中心首页  orderType： 1有效订单 & 2无效订单
  distributsuperior(params) {
    distributsuperior(params).then(({
      status,
      result,
      msg
    }) => {
      if (status == 1) {
        this.setData({

        })
      } else {
        app.wxAPI.alert(msg);
      }
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let token = app.token;
    if (!token) {
      app.wxAPI.alert('未登录!')
        .then(() => {
          wx.reLaunch({
            url: '/pages/USER/user/user?from=pages/HOME/fenxiao/fenxiao'
          })
        })
      return
    }
    this.indexTeam()
  },

})
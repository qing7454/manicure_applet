// pages/HOME/fenxiao/fenxiao.js
import { distributIndexShare, achievement } from '../../../services/API'
import { dalay } from '../../../utils/utils'

const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    http: app.http,
    host: app.host,
    distribut: {},
    type: '',
    offset: '',
    order: [],
    orders: [],
    activeType: 0,
    txalert: false,
    load: true,
    navs: [
      {
        title: '全部订单',
        type: '0',
      },
      {
        title: '已完成订单',
        type: '1',
      },
      {
        title: '未完成订单',
        type: '2',
      }
    ],
  },
  tabList(e) {
    let token = app.token;
    if (!token) {
      app.wxAPI.alert('未登录!')
      return
    }
    const index = e.currentTarget.dataset.index;
    const items = this.data.items;
    wx.navigateTo({
      url: items[index].url
    })
  },
  tabNavBar(e) {
    let type = ''
    type = e.currentTarget.dataset.type;
    console.log(this.data.type)
    console.log(this.data.order)
    this.reSet();
    this.setData({
      activeType: type
    });
    this.distributIndexShare({ user_id: app.userInfo.user_id, type: this.data.activeType, offset: '1' })
  },




  // 分享中心首页  type：0全部 & 1有效订单 & 2无效订单
  distributIndexShare(params) {
    distributIndexShare(params).then(({ errcode, data, errmsg }) => {
      if (errcode == "0" && data!=null) {
        const order = data
        const orders = this.data.orders
        const orderall = orders.concat(order)
        this.setData({
          orders: orderall
        })
        if (order.length < 8) {

          this.setData({
            load: false
          })
          console.log(this.data.load)
          
        }
        console.log(this.data.orders)
      } else {
        //app.wxAPI.alert(errmsg);
      }
    })
  },
  reSet() {
    this.setData({
      orders: []
    })
  }
  ,
  onLoad: function () {
    this.reSet();
    this.setData({
      type: '0',
      offset: '1'
    })
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.reSet();
    // let token = app.token;
    // if (!token) {
    //   app.wxAPI.alert('未登录!')
    //     .then(() => {
    //       wx.reLaunch({
    //         url: '/pages/USER/user/user?from=pages/HOME/fenxiao/fenxiao'
    //       })
    //     })
    //   return
    // }
    this.distributIndexShare({ user_id: app.userInfo.user_id, type: this.data.type, offset: this.data.offset });

  },
  onReachBottom() {
    if (this.data.load == true) {
      const orders = this.data.orders
      const offset = orders.length + 1
      console.log(offset)
      this.distributIndexShare({ user_id: app.userInfo.user_id, type: this.data.type, offset: offset })
    }
  }
})
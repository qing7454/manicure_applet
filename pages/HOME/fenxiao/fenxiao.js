// import { distributWd, accountapply, accountindex, distributIndex } from '../../../services/API'
// import { dalay } from '../../../utils/utils'

// const app = getApp()
// Page({
//   data: {
//     userInfo: app.userInfo,
//     http: app.http,
//     host: app.host,
//     userid: '',
//     amount: '',
//     distribut: {},
//     orders: {},
//     activeType: 0,
//     txalert: false,
//     navs: [{
//       title: '全部订单',
//       type: '0',
//     },
//     {
//       title: '有效订单',
//       type: '1',
//     },
//     {
//       title: '失效订单',
//       type: '2',
//     }
//     ],
//     items: [
//       {
//         name: '分享商品',
//         src: '../../../images/fenxiao_dd.png',
//         url: '/pages/HOME/fxorder/fxorder'
//       },
//       {
//         name: '提现明细',
//         src: '../../../images/fenxiao_tx.png',
//         url: '/pages/HOME/tixian/tixian'
//       },
//       {
//         name: '我的团队',
//         src: '../../../images/fenxiao_td.png',
//         url: '/pages/HOME/tuandui/tuandui'
//       },
//     ]

//   },

//   navigatorTo(e) {
//     let url = e.currentTarget.dataset.url;
//     const id = e.currentTarget.dataset.id;
//     if (!url) {
//       url = `/pages/SPECIAL/special/special?id=${id}`
//     }
//     wx.navigateTo({
//       url: url
//     })
//     this.closePopup();
//   },

//   tabList(e) {
//     let token = app.token;
//     if (!token) {
//       app.wxAPI.alert('未登录!')
//       return
//     }

//     const index = e.currentTarget.dataset.index;
//     const items = this.data.items;
//     wx.navigateTo({
//       url: items[index].url
//     })
//   },
//   tabNavBar(e) {
//     let type = ''
//     type = e.currentTarget.dataset.type;
//     this.setData({
//       activeType: type
//     });

//   },
//   // 分销商验证
//   distributIndex(params) {
//     distributIndex(params).then(({ errcode, status, data, errmsg }) => {

//       // console.log(errcode, status, data, errmsg)
//       if (errcode == "100003") {
//         this.setData({
//           amount: data.amount
//         })
//       }
//       if (errcode == "100001") {
//         app.wxAPI.alert('您还不是分销商，请先申请!')
//           .then(() => {
//             wx.reLaunch({
//               url: '/pages/HOME/shengqing/shengqing'
//             })
//           })
//         return
//       }
//     })
//   },
//   // 申请提现 
//   tixianalert: function () {
//     this.setData({
//       txalert: true
//     })
//   },
//   accountapply(params) {
//     accountapply(params).then(({
//       status,
//       result,
//       msg
//     }) => {
//       if (status == 1) {
//         app.wxAPI.alert(msg);
//       } else {
//         app.wxAPI.alert(msg);
//       }
//     })
//   },
//   /**
//    * 生命周期函数--监听页面加载
//    */
//   onLoad: function (options) {
//     let token = app.token;
//     if (app.token) {
//       this.setData({
//         userInfo: app.userInfo,
//         userid: app.userInfo.user_id
//       });
//       console.log(this.data.userInfo)
//       this.distributIndex({ user_id: this.data.userid })
//     }
//     if (!token) {
//       app.wxAPI.alert('未登录!')
//         .then(() => {
//           wx.reLaunch({
//             url: '/pages/USER/user/user?from=pages/CART/cart/cart'
//           })
//         })
//       return
//     } else {
//       this.setData({
//         userInfo: app.userInfo,
//         userid: app.userInfo.user_id
//       })
//       this.distributIndex({ user_id: this.data.userid })
//     }
//   },

 
//   gbtixian: function () {
//     this.setData({
//       txalert: false
//     })
//   },
//   formSubmit: function (e) {
//     if (e.detail.value.userName.length == 0 || e.detail.value.yhknum.length == 0 || e.detail.value.yhkname.length == 0) {
//       app.wxAPI.alert("银行卡信息请填写完整！");
//     } else {
//       this.accountapply({
//         realname: e.detail.value.userName,
//         bank_card: e.detail.value.yhknum,
//         bank_name: e.detail.value.yhkname,
//         money: e.detail.value.money
//       })
//       this.setData({
//         txalert: true
//       })
//     }
//   },

//   /**
//    * 生命周期函数--监听页面显示
//    */
//   onShow: function () {
//     let token = app.token;
//     if (!token) {
//       app.wxAPI.alert('未登录!')
//         .then(() => {
//           wx.reLaunch({
//             url: '/pages/USER/user/user?from=pages/HOME/fenxiao/fenxiao'
//           })
//         })
//       return
//     }
//   }
// })





// https://nuoliferment.meilashidai.cn/index.php?m=api&c=distribut&a=index&is_json=1&token=08c7d6bd2eba70cbd7e76ccb5e9bfe22&user_id=51
// https://nuoliferment.meilashidai.cn/api//newdistribution.Newdistribution/go_distribut?is_json=1&token=1d7e35a3cbb1bf00c8d6857251c7b3b7&user_id=51
// pages/HOME/fenxiao/fenxiao.js
import {
  distributWd,
  distributHash,
  accountapply,
  indexInfo,
  userInfo,
  accountindex,
  distributIndex
} from '../../../services/API'
import {
  dalay
} from '../../../utils/utils'

const app = getApp()
const template = require('../../TEMPLATE/tabBar/tabBar.js');
Page({
  /**
   * 页面的初始数据
   */
  data: {
    userInfo: app.userInfo,
    http: app.http,
    host: app.host,
    userid: '',
    //parent_id,
    amount: '',
    distribut: {},
    orders: {},
    activeType: 0,
    txalert: false,
    status:'',
    navs: [{
      title: '全部订单',
      type: '0',
    },
    {
      title: '有效订单',
      type: '1',
    },
    {
      title: '失效订单',
      type: '2',
    }
    ],
    items: [
      {
        name: '分销订单',
        src: '../../../images/fenxiao_dd.png',
        url: '/pages/HOME/fxorder/fxorder'
      },
      {
        name: '提现明细',
        src: '../../../images/fenxiao_tx.png',
        url: '/pages/HOME/tixian/tixian'
      },
      {
        name: '我的团队',
        src: '../../../images/fenxiao_td.png',
        url: '/pages/HOME/tuandui/tuandui'
      },
    ]

  },

  //VIP
  // vipInfo(params) {
  //   vipInfo(params).then(({ status, result, msg }) => {
  //     if (status == 1) {


  //     } else {
  //       app.wxAPI.alert(msg)
  //         .then(() => {
  //           wx.reLaunch({
  //             url: '/pages/USER/user/user'
  //           })
  //         })
  //     }
  //   })
  // },

  navigatorTo(e) {
    let url = e.currentTarget.dataset.url;
    const id = e.currentTarget.dataset.id;
    if (!url) {
      url = `/pages/SPECIAL/special/special?id=${id}`
    }
    wx.navigateTo({
      url: url
    })
    this.closePopup();
    template.tabbar("tabBar", 0, this);
  },

  tabList(e) {
    let token = app.token;
    // if (!token) {
    //   app.wxAPI.alert('未登录!')
    //   return
    // }
    // app.wxAPI.alert('功能暂未开发完整!')
    // return

    const index = e.currentTarget.dataset.index;
    const items = this.data.items;
    wx.navigateTo({
      url: items[index].url
    })
  },
  tabNavBar(e) {
    let type = ''
    type = e.currentTarget.dataset.type;
    this.setData({
      activeType: type
    });
    // this.distributIndex({
    //   orderType: type
    // })
  },
  // 分销商验证
  distributIndex(params) {
    distributIndex(params).then(({ errcode, data, errmsg }) => {
      if (errcode == "100003") {
        this.setData({
          amount: data.withdraw
        })
      }
      if (errcode == "100002") {
        app.wxAPI.alert('分销商正在审核！!')
          .then(() => {
            wx.reLaunch({
              url: '/pages/USER/user/user'
            })
          })
        return
      }
      if (errcode == "100001") {
        app.wxAPI.alert('您还不是分销商，请先申请!')
          .then(() => {
            wx.reLaunch({
              url: '/pages/HOME/shengqing/shengqing'
            })
          })
        return
      }
    })
  },
  // 申请提现 
  accountapply(params) {
    accountapply(params).then(({
      status,
      result,
      message
    }) => {
      if (status == 0) {
        app.wxAPI.alert(message);
        
      } else {
        app.wxAPI.alert(message);
      }
      wx.navigateTo({
        url: 'pages/HOME/fenxiao/fenxiao',//跳转回分销首页
  })
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if (app.token) {
      userInfo({
        token: app.token
      })
        .then(({
          status,
          result,
          msg
        }) => {
          // getApp().userInfo = result;
         getApp().userInfo.user_id = result.user_id
          this.setData({
            userInfo: app.userInfo,
            userid: result.user_id
          });
          this.distributIndex({ user_id: this.data.userid })
        })
    }
    template.tabbar("tabBar", 2, this);
  },


  tixianalert: function() {
    this.setData({
      txalert: true
    })
  },
  gbtixian: function () {
    this.setData({
      txalert: false
    })
  },
  formSubmit: function (e) {
    // if (e.detail.value.userName.length == 0 || e.detail.value.yhknum.length == 0 || e.detail.value.yhkname.length == 0) {
    if (e.detail.value.userName.length == 0 || e.detail.value.yhkname.length == 0) {
      app.wxAPI.alert("信息请填写完整！");
    } else {
      this.accountapply({
        //this.accountapply(params).then({})
        take_name: e.detail.value.userName,
        bank_number: e.detail.value.yhknum,
        bank_name: e.detail.value.yhkname,
        take_money: e.detail.value.money
      })
      this.setData({
        txalert: true,
        //status:
      })
    }
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let token = app.token;
    // if (!token) {
    //   app.wxAPI.alert('未登录!')
    //     .then(() => {
    //       wx.reLaunch({
    //         url: '/pages/USER/user/user?from=pages/HOME/fenxiao/fenxiao'
    //       })
    //     })
    //   return
    // }
    // accountindex()
    // distributIndex().then(({
    //   status,
    //   result,
    //   msg
    // }) => {
    //   colsole.log(msg)
    //   if (status == 1) {
    //     console.log(app.userInfo);
    //     app.userInfo.account = result
    //     console.log(app.userInfo);
    //     this.setData({
    //       userInfo: app.userInfo
    //     })
    //   } else {
    //     app.wxAPI.alert(msg)
    //     wx.reLaunch({
    //       url: '/pages/USER/user/user'
    //     })
    //   }
    // })
    // if (token) {
    //   app.wxAPI.alert('抱歉，您还不是分销商!')
    //     .then(() => {
    //       wx.reLaunch({
    //         url: '/pages/USER/user/user'
    //       })
    //     })
    //   return
    //   indexInfo({ token: app.token })
    //     .then(({ status, result, msg }) => {
    //       getApp().userInfo = result;
    //       this.setData({ userInfo: app.userInfo })
    //     })

    //   // this.vipInfo();

    // } else {
    //   app.wxAPI.alert('未登录!')
    //     .then(() => {
    //       wx.reLaunch({
    //         url: '/pages/USER/user/user?from=pages/CART/cart/cart'
    //       })
    //     })
    //   return
    // }
    // this.distributgetList()
  }
})
import { cutList, cutBanner, userInfo } from '../../../services/API';

const app = getApp();
Page({
  data: {
    http: app.http,
    host: app.host,
    items: [],
    banners: []
  },
  onShow(){
    console.log(data);
    userInfo({
      token: app.token
    })
      .then(({
        status,
        result,
        msg
      }) => {

        // getApp().userInfo = result;
        getApp().userInfo.user_id = result.user_id
        console.log('huanc', result.user_id);
        wx.setStorageSync('id', result.user_id)
        this.setData({
          userInfo: app.userInfo,
          userid: result.user_id
        });
      })
  },
  onLoad() {
    cutBanner()
      .then(({ status, result, msg }) => {
        if (status == 1) {
          this.setData({
            banners: result
          })
        }
      })
      
    cutList()
      .then(({status, result, msg}) => {
        if(status == 1) {
          this.setData({
            items: result
          })
        } else {

        }
      })
  }
})
import { getCut, userInfo } from '../../../services/API';
import { remainTime } from '../../../utils/utils.js';

const app = getApp();
Page({
  data: {
    http: app.http,
    host: app.host,
    userInfo: '',
    showPop: false,
    loginPop: false,
    time: '00:00:00',
    selfId: '',
    cutId: ''
  },
  onLoad(options) {
    this.setData({
      cutId: options.id,
      userInfo: app.userInfo,
      selfId: options.selfId || '',
      name: options.name || '',
      headPic: options.headPic || ''
    })
    let token = app.token;
    if(!token) {
      this.setData({
        loginPop: true
      })
    } else {
      this.getCut();
    }
  },
  // 授权
  bindGetUserInfo(e) {
    var that = this;
    app.auth(() => {
      this.setData({ userInfo: app.userInfo, loginPop: app.loginPop })
      
      userInfo({
        token: app.token
      })
        .then(({
          status,
          result,
          msg
        }) => {
          // getApp().userInfo = result;
          getApp().userInfo.user_id = result.user_id
          wx.setStorageSync('id', result.user_id)
          that.getCut();
          that.setData({
            loginPop: false
          })
        })
    }, this.data.from, (this.data.id || 0), (this.data.itemId || 0), this.data.teamId);
  },

  getCut() {
    let params = {
      cut_id: this.data.cutId,
      self_id: this.data.selfId || '',
      // self_id: 40,
      head_pic: app.userInfo.head_pic,
      name: app.userInfo.nickname
    }
    getCut(params)
      .then(({ status, result, msg }) => {
        if (status == 1) {
          if(!result.cut) {
            this.setData({
              showPop: true,
              cutMoney: result.cut_money,
              selfId: result.self_id
            })
            this.getCut();
          }

          let totalCutPrice = 0;
          (result.help || []).forEach((item) => {
            totalCutPrice += parseFloat(item.cut_money);
          })
          this.setData({
            status: status,
            goods: result.cut,
            cutPrice: result.help,
            totalCutPrice: totalCutPrice.toFixed(2),
            width: (totalCutPrice / parseFloat(result.cut && result.cut.total_price)) * 710
          })
          const endTime = result.cut.end_time;
          // console.log(endTime);
          let timer = setInterval(() => {
            let finish = remainTime(endTime  - new Date().getTime() / 1000 );
            // console.log('x', new Date().getTime());
            if(finish == '结束') {
              timer.clearInterval();
              finish = '00：00：00';
            }
            this.setData({
              time: finish
            })
          }, 1000)
          
        } else if(status == 2) {
          this.setData({
            status: status,
            goods: result
          })
        } else if (status == 3 || status == 4) {
          this.setData({
            status: status,
            goods: result
          })
          const endTime = result.end_time;
          let timer = setInterval(() => {
            let finish = remainTime(endTime * 1000 - new Date().getTime());
            if (finish == '结束') {
              timer.clearInterval();
              finish = '00：00：00';
            }
            this.setData({
              time: finish
            })
          }, 1000)
        } else {
          app.wxAPI.alert(msg)
            .then(() => {
              wx.navigateBack();
            });
        }
      })
  },
  onShow(){
    
    console.log(data);
    userInfo({
      token: app.token
    })
      .then(({
        status,
        result,
        msg
      }) => {

        // getApp().userInfo = result;
        getApp().userInfo.user_id = result.user_id
        console.log('huanc', result.user_id);
        wx.setStorageSync('id', result.user_id)
        this.setData({
          userInfo: app.userInfo,
          userid: result.user_id
        });
      })
  },
  onShareAppMessage(res) {
    console.log(`/pages/CUTPRICE/cutDetail/cutDetail?id=${this.data.cutId}&selfId=${this.data.goods.self_id}&headPic=${app.userInfo.head_pic}&name=${app.userInfo.nickname}`);
    return {
      title: '砍价分享',
      path: `/pages/CUTPRICE/cutDetail/cutDetail?id=${this.data.cutId}&selfId=${this.data.goods.self_id}&headPic=${app.userInfo.head_pic}&name=${app.userInfo.nickname}`
    }
  },
  closePop(e) {
    const info = e.currentTarget.dataset.info;
    if(info) {
      wx.switchTab({
        url: `/pages/HOME/home/home`
      })
    }
    this.setData({
      showPop: false,
      loginPop: false
    })
  },
  sure() {
    const goods = this.data.goods;
    wx.navigateTo({
      url: `/pages/CUTPRICE/payOrder/payOrder?goodsId=${goods.goods_id}&itemId=${goods.item_id || ''}&selfId=${goods.self_id}`
    })
  }
})
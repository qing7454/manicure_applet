import { cartList, delCart, getCouponList,userInfo } from '../../../services/API'
import config from '../../../utils/config'
const app = getApp();
const template = require('../../TEMPLATE/tabBar/tabBar.js');
Page({
  data: {
    txtStyle: '',
    startX: 0,
    moveX: 0,
    endX: 0,
    disX: 0,
    host: config.host,
    items: [],
    total_price: {},
    num: 1,
    checkedLen: 0,
    isChecked: true,
    isNomore: false,
    isShowCoupon: false,
    isEmpty: false,
    special: {
      showSpecial: false,
      list: ''
    }
  },
  onLoad() {
    let special = this.data.special;
    special.list = app.specialList;
    this.setData({ special });

    template.tabbar("tabBar", 2, this)
  },
  onShow() {
    let token = app.token;
    if (!token) {
      app.wxAPI.alert('未登录!')
      .then(() => {
        wx.reLaunch({
          url: '/pages/USER/user/user?from=pages/CART/cart/cart'
        })
      })
      return
    }
    userInfo({
      token: app.token
    })
      .then(({
        status,
        result,
        msg
      }) => {

        // getApp().userInfo = result;
        getApp().userInfo.user_id = result.user_id
        console.log('huanc', result.user_id);
        wx.setStorageSync('id', result.user_id)
        this.setData({
          userInfo: app.userInfo,
          userid: result.user_id
        });
      })
    cartList().then(this.render)
  },
  render({ status, result, msg }) {
    if (status === 1) {
      this.setData({
        items: result.cartList,
        total_price: result.total_price,
        checkedLen: result.cartList.filter(item => item.selected).length
      })
      const is = this.isCheckedAll()
      this.setData({ isChecked: is ? false : true })
      const len = this.data.items.length == 0
      this.setData({ isNomore: len ? true : false })
    } else {
      app.wxAPI.alert(msg)
    }
  },
  checked(e) {
    const index = e.currentTarget.dataset.index
    const obj = this.data.items[index]
    let params = [{
      selected: parseInt(obj.selected) ? 0 : 1,
      cartID: obj.id,
      goodsNum: obj.goods_num,
      user_id: getApp().userInfo.user_id
    }]
    cartList({ cart_form_data: JSON.stringify(params) })
      .then(this.render)
  },
  checkedAll() {
    const is = this.isCheckedAll()
    const arr = this.data.items
    let params = []
    arr.forEach(item => {
      params.push({
        selected: is ? 1 : 0,
        cartID: item.id,
        goodsNum: item.goods_num,
        user_id: getApp().userInfo.user_id
      })
    })
    cartList({ cart_form_data: JSON.stringify(params) })
      .then(this.render)
  },
  // 判断是全选 还是 全反选
  isCheckedAll() {
    const arr = this.data.items
    return !arr.every(({ selected }) => selected == 1)
  },
  del(e) {
    app.wxAPI.confirm('确定删除？')
      .then(() => {
        const index = e.currentTarget.dataset.index
        const obj = this.data.items[index]
        return delCart({ ids: obj.id })
      })
      .then(({ status, result, msg }) => {
        if (status === 1) {
          cartList().then(this.render)
          app.wxAPI.toast('删除成功')
        } else {
          app.wxAPI.alert(msg)
        }
      })
  },
  onblur(e) {
    let num = e.detail.value
    const index = e.currentTarget.dataset.index
    const obj = this.data.items[index]
    let params = [{
      selected: obj.selected,
      cartID: obj.id,
      goodsNum: num <= 0 ? 1 : num
    }]
    cartList({ cart_form_data: JSON.stringify(params) })
      .then(this.render)
  },
  add(e) {
    const index = e.currentTarget.dataset.index
    const obj = this.data.items[index]
    if (obj.goods_num < 99) {
      let params = [{
        selected: obj.selected,
        cartID: obj.id,
        goodsNum: ++obj.goods_num
      }]
      cartList({ cart_form_data: JSON.stringify(params) })
        .then(this.render)
    }
  },
  subtract(e) {
    const index = e.currentTarget.dataset.index
    const obj = this.data.items[index]
    if (obj.goods_num > 1) {
      let params = [{
        selected: obj.selected,
        cartID: obj.id,
        goodsNum: --obj.goods_num
      }]
      cartList({ cart_form_data: JSON.stringify(params) })
        .then(this.render)
    }
  },
  settlement() {
    if (this.data.checkedLen <= 0) {
      return app.wxAPI.alert('尚选中任何商品')
    }
    wx.navigateTo({ url: '/pages/CART/payOrder/payOrder' })
  },
  end(e) {
    let endX = e.changedTouches[0].clientX
    let items = this.data.items
    let index = e.currentTarget.dataset.index
    if (this.data.disX > 40) {
      items[index]['txtStyle'] = 'transform: translateX(-13%); transition: transform 0.1s ease-in;'
    } else if (this.data.disX < -30) {
      items[index]['txtStyle'] = 'transform: translateX(0); transition: transform 0.1s ease-in;'
    }
    this.setData({ items: items })
  },
  jumpDetail(e) {
    const id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: `/pages/KILL/detail/detail?id=${id}`
    })
  },
  //专题点击
  clickSpecial() {
    template.tabbar("tabBar", 2, this);
    let special = this.data.special;
    if (special.list.length == 0) {
      app.wxAPI.alert('专题暂未开启！')
        .then(() => {
          template.tabbar("tabBar", 3, this);
        })
      return;
    }
    special.showSpecial = true;
    this.setData({ special });
  },
  navigatorTo(e) {
    let url = e.currentTarget.dataset.url;
    const id = e.currentTarget.dataset.id;
    if (!url) {
      url = `/pages/SPECIAL/special/special?id=${id}`
    }
    wx.navigateTo({
      url: url
    })
    this.closePopup();
    template.tabbar("tabBar", 3, this);
  },
  closePopup() {
    let special = this.data.special;
    special.showSpecial = false;
    this.setData({ special });
    template.tabbar("tabBar", 3, this);
  }

})

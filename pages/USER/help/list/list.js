// pages/USER/help/help.js
import { articlegetCategory, getArticleList, articleinfo } from '../../../../services/API';
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    onelist: [],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options)
    getArticleList({ cate: options.article_id }).then(({ status, result, msg }) => {
      if (status == 1) {
        this.setData({
          onelist: result.data,

        })




        // wx.setStorage({
        //   key: 'authorize',
        //   data: true,
        // })

        // let arr = this.data.items;
        // arr.forEach(item => {
        //   if (item.id == 5) {
        //     item.flag = result == 2 ? true : false
        //   }
        // })

      } else {
        app.wxAPI.alert(msg)
      }
    })

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
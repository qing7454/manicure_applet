// pages/USER/help/help.js
import { articlegetCategory, getArticleList, articleinfo } from '../../../services/API';
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    category: [],
    onelist: [],
    twolist: [],

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    articlegetCategory().then(({ status, result, msg }) => {
      if (status == 1) {

        this.setData({
          // category: result.category,
          // onelist: result.list
          category: result
        })

        // wx.setStorage({
        //   key: 'authorize',
        //   data: true,
        // })

        // let arr = this.data.items;
        // arr.forEach(item => {
        //   if (item.id == 5) {
        //     item.flag = result == 2 ? true : false
        //   }
        // })

      } else {
        app.wxAPI.alert(msg)
      }
    })
  console.log('category',this.data.category)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
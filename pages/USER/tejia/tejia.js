import {offPrice,thirdLogin,getActivity } from '../../../services/API'
import { remainTime } from '../../../utils/utils';
import { auth } from '../../../services/auth';
import * as wxAPI from '../../../services/wxAPI';

const app = getApp();
Page({
  data: {
    http: app.http,
    host: app.host,
    offprice: [],
    isAgain: true,
    isNomore: false
  },
  onLoad() {
    app.wxAPI.showLoading('加载中...');
    this.getoffPrice();
    let userInfo = '';
    wxAPI.getSetting()
      .then((res) => {
        if (~res.indexOf('scope.userInfo')) {
          wxAPI.getUserInfo()
            .then((res) => {
              userInfo = res.userInfo;
              return wxAPI.login()
            })
            .then((res) => {
              return thirdLogin({
                code: res.code,
                from: 'miniapp',
                nickname: userInfo.nickName,
                head_pic: userInfo.avatarUrl
              })
            })
            .then(({ status, result, msg }) => {
              if (status == 1) {
                result.nickname = userInfo.nickName;
                app.userInfo = result;
                app.token = result.token;
              }
            })
          return;
        }
      })
  },
  getoffPrice(){
    offPrice().then(({ status, result, msg }) => {
      this.setData({
        offprice: result,
        loading: false
      })
      let loading = this.data.loading;
      let homeLoading = this.data.homeLoading;
      if (!loading && !homeLoading) {
        app.wxAPI.hideLoading();
      }
      const products = result || [];
      const arr = this.data.products;
      if (status == 1) {
        this.setData({ 
          offprice: arr,
          isAgain: true
         })
         this.finish(products)
      } else {
        app.wxAPI.alert(msg);
      }
    })
  },
  onblur(e) {
    this.setData({
      keyword: e.detail.value
    })
  },
  oninput(e) {
    let keyword = e.detail.value
    if (keyword != '') {
      this.setData({
        isEmpt: true,
        keyword
      })
    } else {
      this.setData({
        isEmpt: false
      })
    }
    return keyword.trim()
  },
  empt() {
    setTimeout(() => {
      this.setData({
        keyword: '',
        isEmpt: false
      })
    }, 50)
  },
  search() {
    const keyword = this.data.keyword;
    if (keyword) {
      wx.navigateTo({
        url: `/pages/HOME/searchResult/searchResult?keyword=${keyword}`
      })
    }
  },
  tabList(e) {
    const index = e.currentTarget.dataset.index;
    const list = this.data.navTabs;
    if (!list[index].url) {
      app.wxAPI.alert('该功能未开启！');
      return;
    } else {
      wx.navigateTo({
        url: list[index].url
      })
    }
  },
  onShareAppMessage(res) {
    return {
      title: '卤味',
      path: '/pages/HOME/home/home'
    }
  },
  navigatorTo(e) {
    let url = e.currentTarget.dataset.url;
    const id = e.currentTarget.dataset.id;
    if (!url) {
      url = `/pages/SPECIAL/special/special?id=${id}`
    }
    wx.navigateTo({
      url: url
    })
    template.tabbar("tabBar", 0, this);
  },
  //判断是否结束
  finish(arr) {
    if (arr.length < 10) {
      this.setData({
        isAgain: false,
        isNomore: true
      })
    }
  },
  onReachBottom() {
    if (!dalay(1000)) return;
    if (!this.data.isAgain) return;
    this.setData({ isAgain: false });
  },
})
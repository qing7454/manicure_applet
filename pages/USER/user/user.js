import { teamList, userInfo } from '../../../services/API';
import { auth } from '../../../services/auth';

const app = getApp();
const template = require('../../TEMPLATE/tabBar/tabBar.js');
Page({
  data: {
    userInfo: '',
    from: '',
    id: '',
    userNav: [
      {
        type: 'WAITPAY',
        src: '../../../images/icon_user_pay.png',
        name: '待付款'
      },
      {
        type: 'WAITSEND',
        src: '../../../images/icon_user_pend.png',
        name: '待发货'
      },
      {
        type: 'WAITRECEIVE',
        src: '../../../images/icon_user_goods.png',
        name: '待收货'
      },
      {
        type: 'WAITCCOMMENT',
        src: '../../../images/icon_user_finish.png',
        name: '待评论'
      },
      {
        type: 'TEAM',
        src: '../../../images/icon_user_refund.png',
        name: '退款/售后'
      }
    ],
    items: [
      // {
      //   name: '我的秒杀',
      //   url: '/pages/KILL/kill/kill',
      //   src: '../../../images/icon_user_ms.png'
      // },
      {
        name: '我的拼团',
        url: '/pages/TEAM/order/order',
        src: '../../../images/icon_user_tuan.png'
      },
      // {
      //   name: '分销中心',
      //   url: '/pages/HOME/fenxiao/fenxiao',
      //   src: '../../../images/icon_user_fx.png'
      //   // fenxiao: 0,
      // },
      {
        name: '我的收藏',
        url: '/pages/USER/collect/collect',
        src: '../../../images/icon_user_sc.png'
      },
      {
        name: '帮助中心',
        url: '/pages/USER/help/help',
        src: '../../../images/icon_user_bz.png'
      },
      {
        name: '我的优惠券',
        url: '/pages/USER/notused/notused',
        src: '../../../images/icon_user_yh.png'
      },
    ],
    orders: [
      
      {
        name: '关于我们',
        url: '/pages/USER/notused/notused',
        src: '../../../images/icon_user_gy.png'
      },
      
    ],
    special: {
      showSpecial: false,
      list: []
    }
  },
  onLoad(options) {
    let special = this.data.special;
    special.list = app.specialList;
    this.setData({ special });

    template.tabbar("tabBar", 3, this);
    this.setData({
      userInfo: app.userInfo,
      from: options.from || '',
      id: options.id || '',
      itemId: options.item_id || '',
      teamId: options.team_id || ''
    });
    let token = app.token;
    if (!token) {
      auth(() => {
        this.setData({ userInfo: app.userInfo })
      })
    }
  },
  onShow() {
    userInfo({
      token: app.token
    })
    .then(({
      status,
      result,
      msg
    }) => {
      
      // getApp().userInfo = result;
      getApp().userInfo.user_id = result.user_id
      console.log('huanc', result.user_id);
      wx.setStorageSync('id', result.user_id)
      this.setData({
        userInfo: app.userInfo,
        userid: result.user_id
      });
    })
  },
  openUserNav(e) {
    let token = app.token;
    if (!token) {
      app.wxAPI.alert('未登录!')
      return
    }
    const type = e.currentTarget.dataset.type;
    if (type == 'TEAM') {
      wx.navigateTo({
        url: `/pages/USER/refund/refund`
      })
    } else {
      wx.navigateTo({
        url: `/pages/USER/order/order?type=${type}`
      })
    }
  },
  tabList(e) {
    let token = app.token;
    if (!token) {
      app.wxAPI.alert('未登录!')
      return
    }
    const index = e.currentTarget.dataset.index;
    const items = this.data.items;
    wx.navigateTo({
      url: items[index].url
    })
  },
  tapOrder(e) {
    let token = app.token;
    if (!token) {
      app.wxAPI.alert('未登录!')
      return
    }
    const index = e.currentTarget.dataset.index;
    const orders = this.data.orders;
    wx.navigateTo({
      url: orders[index].url
    })
  },

  // 授权
  bindGetUserInfo(e) {
    app.auth(() => {  
      this.onShow();
      this.setData({ userInfo: app.userInfo })
    }, this.data.from, (this.data.id || 0), (this.data.itemId || 0), this.data.teamId)
    console.log(this.data.userInfo.nickname)
  },

  //专题点击
  clickSpecial() {
    template.tabbar("tabBar", 2, this);
    let special = this.data.special;
    if (special.list.length == 0) {
      app.wxAPI.alert('专题暂未开启！')
        .then(() => {
          template.tabbar("tabBar", 4, this);
        })
      return;
    }
    special.showSpecial = true;
    this.setData({ special });
  },
  navigatorTo(e) {
    let url = e.currentTarget.dataset.url;
    const id = e.currentTarget.dataset.id;
    if (!url) {
      url = `/pages/SPECIAL/special/special?id=${id}`
    }
    wx.navigateTo({
      url: url
    })
    this.closePopup();
    template.tabbar("tabBar", 4, this);
  },
  closePopup() {
    let special = this.data.special;
    special.showSpecial = false;
    this.setData({ special });
    template.tabbar("tabBar", 4, this);
  }
})